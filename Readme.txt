The test was built using protractor framework.

To run the test you need install the protractor in your machine using:
npm install -g protractor
webdriver-manager update

Run "protractor conf.js" on qa-test folder.

For more information http://www.protractortest.org/#/tutorial