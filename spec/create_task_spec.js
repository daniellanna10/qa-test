var TaskPage = function() {

  var myTasks = element(by.css('.navbar-collapse')).element(by.css('.navbar-nav')).element(by.linkText('My Tasks'));
  var messageTask = element(by.css('.container')).element(by.tagName('h1'));
  var taskBody = element(by.model('newTask.body'));
  var addTaskButton = element(by.css('.input-group-addon'));
  var todoList;
  var tasksBody;

  this.get = function() {
    browser.get('http://qa-test.avenuecode.com/');
  };

  this.clickMyTasks = function() {
    myTasks.click();
  };

  this.getMessageTask = function(){
    messageTask.getText();
  }

  this.setTaskBody = function(val) {
    taskBody.sendKeys(val);
  };

  this.getTodoList = function() {
    return todoList = element.all(by.repeater('task in tasks'));
  };

  this.getTasksBody = function() {
    return tasksBody = element.all(by.binding('task.body'));
  };

  this.clickAddTaskButton = function() {
    addTaskButton.click();
  };
};

var LoginPage = function() {

  var emailInput = element(by.id("user_email"));
  var passwordInput = element(by.id("user_password"));
  var signIn = element(by.css('.navbar-right')).element(by.tagName('a'));
  var loginButton  = element(by.css('.btn-primary'));

  this.get = function() {
    browser.get('http://qa-test.avenuecode.com/users/sign_in');
  };

  this.setEmailInput = function(val) {
    emailInput.sendKeys(val);
  };

  this.setPasswordInput = function(val) {
    passwordInput.sendKeys(val);
  };

  this.clickSignIn = function() {
    signIn.click();
  };

  this.clickLoginButton = function() {
    loginButton.click();
  };
};


describe('Create Task', function() {
  var loginPage;
  var taskPage;
  beforeEach(function() {
    loginPage = new LoginPage();
    taskPage = new TaskPage();
  });

  it('should login', function() {

    loginPage.get();

    loginPage.clickSignIn();

    loginPage.setEmailInput("daniel_lanna10@hotmail.com");

    loginPage.setPasswordInput("12345678");

    loginPage.clickLoginButton();

    expect(browser.getCurrentUrl()).toEqual('http://qa-test.avenuecode.com/');
  });

  it('should see my tasks', function() {

    taskPage.clickMyTasks();

    expect(browser.getCurrentUrl()).toEqual('http://qa-test.avenuecode.com/tasks');
  });

  it('should show message my tasks', function() {

    expect(taskPage.getMessageTask()).toEqual("Hey Daniel, this is your todo list for today.");

  });

  it('should add and show a tasks ', function() {

    taskPage.setTaskBody("Task 1");
    taskPage.clickAddTaskButton();

    expect(taskPage.getTodoList().count()).toBeGreaterThan(0);

  });

  it('should require at least three characters', function() {

    taskPage.setTaskBody("Ta");
    taskPage.clickAddTaskButton();

    expect(taskPage.getTasksBody().getText().first().getText()).not.toBe("Ta");

  });

  it('should not have more than 250 characters', function() {

    taskPage.setTaskBody("Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters !!!!!!!!!!!!!");

    taskPage.clickAddTaskButton();

    expect(taskPage.getTasksBody().getText().first().getText()).not.toBe("Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters " +
        "Task more than 250 characters !!!!!!!!!!!!!");
  });
});
