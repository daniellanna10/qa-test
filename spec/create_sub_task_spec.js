var SubTaskPage = function() {

  var myTasks = element(by.css('.navbar-collapse')).element(by.css('.navbar-nav')).element(by.linkText('My Tasks'));
  var messageTask = element(by.css('.container')).element(by.tagName('h1'));
  var taskBody = element(by.model('newTask.body'));
  var addTaskButton = element(by.css('.input-group-addon'));
  var todoList;
  var subTaskBody = element(by.id('new_sub_task'));
  var dateTask = element(by.model('subTask.due_date'));
  var addSubTaskButton = element(by.id('add-subtask'));
  var closeModal = element(by.css('.modal-footer')).element(by.css('.btn-primary'));
  var subTasks;
  var subTaskBody;

  this.get = function() {
    browser.get('http://qa-test.avenuecode.com/');
  };

  this.clickMyTasks = function() {
    myTasks.click();
  };

  this.getMessageTask = function(){
    messageTask.getText();
  }

  this.setTaskBody = function(val) {
    taskBody.sendKeys(val);
  };

  this.setSubTaskBody = function(val) {
    subTaskBody.sendKeys(val);
  };

  this.setDateTask = function(val) {
    dateTask.sendKeys(val);
  };

  this.getSubTasksBody = function() {
    return subTaskBody = element.all(by.binding('sub_task.body'));
  };

  this.getTodoList = function() {
    return todoList = element.all(by.repeater('task in tasks'));
  };

  this.getSubTasks = function() {
    return subTasks = element.all(by.repeater('sub_task in task.sub_tasks'));
  };

  this.clickAddTaskButton = function() {
    addTaskButton.click();
  };

  this.clickAddSubTaskButton = function() {
    addSubTaskButton.click();
  };

  this.closeModal = function() {
    closeModal.click();
  };

  this.openModal = function() {
    this.getTodoList().get(0).element(by.tagName('button')).click();
  };
};

describe('Create Sub Task', function() {
  var subTaskPage;
  beforeEach(function() {
    subTaskPage = new SubTaskPage();
  });

  it('user should see a button labeled as Manage Subtasks', function() {

    subTaskPage.setTaskBody("Task 2");
    subTaskPage.clickAddTaskButton();

    expect(subTaskPage.getTodoList().first().getText()).toContain("Manage Subtasks");
  });

  it('The button should have the number of subtasks created for that tasks�', function() {

    subTaskPage.setTaskBody("Task 3");
    subTaskPage.clickAddTaskButton();

    expect(subTaskPage.getTodoList().first().getText()).toContain("(0)");

    subTaskPage.setTaskBody("Task 4");
    subTaskPage.clickAddTaskButton();

    subTaskPage.openModal();

    subTaskPage.setSubTaskBody("Sub Task for Task 4");
    clear(element(by.model('subTask.due_date')));
    subTaskPage.setDateTask("01/01/2015");
    subTaskPage.clickAddSubTaskButton();
    subTaskPage.closeModal();

    expect(subTaskPage.getTodoList().first().getText()).toContain("1");
  });

  it('should have a read only ID and the task description field', function() {

    subTaskPage.openModal();

    expect(element(by.id('edit_task')).getAttribute("readonly")).not.toBe(null);

    expect(element(by.css('.modal-header')).element(by.css('.modal-title')).getText()).toContain("Editing Task");
  });

  it('should can enter the SubTask Description max (250 characters)', function() {

    subTaskPage.setSubTaskBody("A Sub Task with more than 250 characters " +
        "A Sub Task with more than 250 characters " +
        "A Sub Task with more than 250 characters " +
        "A Sub Task with more than 250 characters " +
        "A Sub Task with more than 250 characters " +
        "A Sub Task with more than 250 characters !!!!!!!");

    clear(element(by.model('subTask.due_date')));
    subTaskPage.setDateTask("01/01/2015");
    subTaskPage.clickAddSubTaskButton();

    expect(subTaskPage.getSubTasksBody().getText().first().getText()).not.toBe("A Sub Task with more than 250 characters " +
        "A Sub Task with more than 250 characters " +
        "A Sub Task with more than 250 characters " +
        "A Sub Task with more than 250 characters " +
        "A Sub Task with more than 250 characters " +
        "A Sub Task with more than 250 characters !!!!!!!");

  });

  it('should can enter SubTask due date (MM/dd/yyyy format)', function() {

    subTaskPage.setSubTaskBody("Sub Task");
    clear(element(by.model('subTask.due_date')));
    subTaskPage.setDateTask("31/02/2015");
    subTaskPage.clickAddSubTaskButton();

    expect(subTaskPage.getSubTasksBody().getText().first().getText()).not.toBe("Sub Task");

  });

  it('should fail on required fields', function() {

    var subTasksBefore = subTaskPage.getSubTasks().count();

    subTaskPage.setSubTaskBody("");

    clear(element(by.model('subTask.due_date')));

    subTaskPage.clickAddSubTaskButton();

    var subTasksAfter = subTaskPage.getSubTasks().count();

    expect(subTasksBefore).toEqual(subTasksAfter);

  });
});


function clear(elem, length) {
  length = length || 100;
  var backspaceSeries = '';
  for (var i = 0; i < length; i++) {
    backspaceSeries += protractor.Key.BACK_SPACE;
  }
  elem.sendKeys(backspaceSeries);
}